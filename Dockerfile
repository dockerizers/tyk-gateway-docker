FROM tykio/tyk-gateway:v5.0.0
LABEL maintainer="Kariuki Gathitu <kgathi2@gmail.com>"
LABEL version="1.0"

# openvpn | procps -> ps -ef tyk [k8 liveness probe] | iputils-ping -> ping|
ENV CORE_PACKAGES="openvpn procps iputils-ping"

RUN apt-get update && apt-get install -y $CORE_PACKAGES && apt-get clean
