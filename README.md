# Tyk Docker Image with VPN and Git setup

This Repo has two branches. `master` and `release`. Once a release branch is updated, the docker image should be built and pushed to the gitlab docker registry. 


Image: registry.gitlab.com/dockerizers/tyk-docker-image

```yaml
environment:
    - CLUSTER_REPO_USER=
    - CLUSTER_REPO_PASSWORD=
    - CLUSTER_REPO=
    - CLUSTER_REPO_BRANCH=
    - TYK_ENV=
    - TYKCONF=
    - TYK_GW_SECRET=
    - TYK_GW_NODESECRET=
    - VPN_CONF=
    - GD_Key=$GD_Key
    - GD_Secret=$GD_Secret
    - LE_CONFIG_HOME=/etc/volume/acme.sh
    - GATEWAY_DOMAIN=dev.qwwik.com

entrypoint:
    - bash
    - -c
    - |
      openvpn --daemon --config $$VPN_CONF
      service telegraf start
      git clone https://$$CLUSTER_REPO_USER:$$CLUSTER_REPO_PASSWORD@$$CLUSTER_REPO /etc/app-cluster --depth 1 -b $$CLUSTER_REPO_BRANCH
      /root/.acme.sh/acme.sh --issue --dns dns_gd -d $${GATEWAY_DOMAIN} -d www.$${GATEWAY_DOMAIN}
      /etc/tyk-gateway/git-repo-watcher -d /etc/app-cluster &
      /opt/tyk-gateway/tyk --conf=$${TYKCONF}


volumes:
    - ./secrets/vpn_client.conf:/etc/openvpn/clients/vpn_client.conf
    - ./secrets/gateway/ssl:/etc/certs
    - ./config/tyk.conf:/opt/tyk-gateway/tyk.conf
    - ./config/telegraf/gateway.conf:/etc/telegraf/telegraf.conf:ro
    - ./volumes/gateway/acme.sh:/etc/volume/acme.sh

```
     
# Let's Encrypt Acme.sh script
https://github.com/acmesh-official/acme.sh

```dockerfile
RUN wget -O -  https://get.acme.sh | sh
```

Set up godaddy Keys from [here](https://github.com/acmesh-official/acme.sh/wiki/dnsapi#4-use-godaddycom-domain-api-to-automatically-issue-cert)

Once done on your local host export the GoDaddy api credentials as ENV variables so that docker compose can pick them up when starting

```bash
export GD_Key="8df7a0987df98adsf"
export GD_Secret="a70df98dsf7"
```

You can add a file `~/.bash_env` with the above content and then append `~/.bash_profile` with the following
```bash
...
source ~/.bash_env
```

# Image Versioning
When pushing a new image, ensure that teh VERSION variable in `.gitlab-ci` matches the docker image version.


# Hot realoading
In a docker or kubernetes setup, hot reloading via `kill -SIGUSR2 $(cat "/var/run/tyk/tyk-gateway.pid")` also kills the container as the attached process is the tyk.pid process. Therefore, best way without reloading the contaier is via api 
```bash
curl -X GET https://127.0.0.1/tyk/reload/ \
    -H "X-Tyk-Authorization: $TYK_GW_SECRET" -k
```

Git repo watcher
https://github.com/kolbasa/git-repo-watcher

Gcloud
```dockerfile
# Add gcloud sdk at /google-cloud-sdk/bin/gcloud
ENV GCLOUD_ARCHIVE=google-cloud-sdk-284.0.0-linux-x86_64.tar.gz
RUN cd / && curl -O https://storage.googleapis.com/cloud-sdk-release/$GCLOUD_ARCHIVE && \
    tar -xf $GCLOUD_ARCHIVE && \
    rm $GCLOUD_ARCHIVE
```
